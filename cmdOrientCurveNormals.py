import sys
import math

import maya.api.OpenMaya as OpenMaya

import maya.cmds as mc


kPluginName     = 'spOrientCurveNormals'


################################################################################
# @brief      Tells maya that the new API are used
#
def maya_useNewAPI():
    pass


################################################################################
# @brief      Class for sp orient curve normals.
#
# @details    This custom class takes the selected curve and the selected mesh
#             and orients the curve normals on the mesh normals.
#
class spOrientCurveNormals(OpenMaya.MPxCommand):

    kNurbsFlag              = '-n'
    kNurbsLongFlag          = '-nurbs'

    kMeshFlag               = '-m'
    kMeshLongFlag           = '-mesh'

    kMultiplierFlag         = '-l'
    kMultiplierLongFlag     = '-multiplier'

    inputNurbs              = None
    inputMesh               = None
    inputMultiplier         = 1.0

    ###############################################################################
    # @brief      Constructs the object.
    #
    def __init__(self):
        super(spOrientCurveNormals, self).__init__()

    ############################################################################
    # @brief      Command called when the spOrientCurveNormals is executed
    #
    # @param      self  The object
    # @param      args  The arguments
    #
    #
    def doIt(self, args):
        self.parseArguments(args)
        self.redoIt()

    ############################################################################
    # @brief      Actual execution of the command
    # @param      self  The object
    #
    def redoIt(self):
        selInputNurbs = OpenMaya.MGlobal.getSelectionListByName(self.inputNurbs)
        selInputMesh = OpenMaya.MGlobal.getSelectionListByName(self.inputMesh)

        curveDagPath = selInputNurbs.getComponent(0)[0]
        meshDagPath = selInputMesh.getComponent(0)[0]

        if curveDagPath.hasFn(OpenMaya.MFn.kNurbsCurve):
            curveMFn = OpenMaya.MFnNurbsCurve(curveDagPath)
        else:
            sys.stderr.write("The curve specified is of a wrong type")
            raise

        if meshDagPath.hasFn(OpenMaya.MFn.kMesh):
            meshMFn = OpenMaya.MFnMesh(meshDagPath)
        else:
            sys.stderr.write("The mesh specified is of a wrong type")
            raise

        pointArray = OpenMaya.MPointArray()
        knotsArray = OpenMaya.MDoubleArray()

        for curvePoint in curveMFn.cvPositions():
            normal, faceID = meshMFn.getClosestNormal(curvePoint)
            point = curvePoint + normal * self.inputMultiplier
            pointArray.append(point)

        for i in xrange(0, curveMFn.numKnots):
            knotsArray.append(i)

        newCurve = OpenMaya.MFnNurbsCurve()
        nullObj = OpenMaya.MObject()
        try:
            newCurve.create(pointArray, knotsArray, 3, OpenMaya.MFnNurbsCurve.kOpen, 0, 0, nullObj)
        except:
            sys.stderr.write("Error creating the new curve\n")
            raise

    ############################################################################
    # @brief      Undo the executed command restoring the scene.
    # 
    # @param      self  The object
    #
    def undoIt(self):
        pass

    ############################################################################
    # @brief      Determines if undoable.
    # @param      self  The object
    # @return     True if undoable, False otherwise.
    #
    def isUndoable(self):
        return True

    ###############################################################################
    # @brief      Parse arguments for the command
    #
    # @param      self  The object
    # @param      args  The arguments
    #
    def parseArguments(self, args):
        argData = OpenMaya.MArgParser(self.syntax(), args)

        if argData.isFlagSet(self.kNurbsFlag):
            self.inputNurbs = argData.flagArgumentString(self.kNurbsFlag, 0)

        if argData.isFlagSet(self.kMeshFlag):
            self.inputMesh = argData.flagArgumentString(self.kMeshFlag, 0)

        if argData.isFlagSet(self.kMultiplierFlag):
            self.inputMultiplier = argData.flagArgumentDouble(self.kMultiplierFlag, 0)

    ###############################################################################
    # @brief      Creates a new instance for the current object
    # @return     new instance for spOrientCurveNormals
    #
    @staticmethod
    def cmdCreator():
        return spOrientCurveNormals()

    ###############################################################################
    # @brief      Sets the syntax.
    # @return     The MSyntax object with all the syntaxes defined
    #
    @staticmethod
    def setSyntax():
        msyntax = OpenMaya.MSyntax()
        msyntax.addFlag(spOrientCurveNormals.kNurbsFlag, spOrientCurveNormals.kNurbsLongFlag, OpenMaya.MSyntax.kString)
        msyntax.addFlag(spOrientCurveNormals.kMeshFlag, spOrientCurveNormals.kMeshLongFlag, OpenMaya.MSyntax.kString)
        msyntax.addFlag(spOrientCurveNormals.kMultiplierFlag, spOrientCurveNormals.kMultiplierLongFlag, OpenMaya.MSyntax.kDouble)
        return msyntax


################################################################################
# @brief      Initialize the custom command
# @param      mobject  The mobject
#
def initializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject, "Andrea Rastelli", "1.0", "2016")

    try:
        mplugin.registerCommand(kPluginName, spOrientCurveNormals.cmdCreator, spOrientCurveNormals.setSyntax)
    except:
        sys.stderr.write("Failed to register command: %s\n" % kPluginName)
        raise


################################################################################
# @brief      Uninitialize the custom command
# @param      mobject  The mobject
#
def uninitializePlugin(mobject):
    mplugin = OpenMaya.MFnPlugin(mobject)

    try:
        mplugin.deregisterCommand(kPluginName)
    except:
        sys.stderr.write("Failed to unregister command: %s\n" % kPluginName)
        raise
